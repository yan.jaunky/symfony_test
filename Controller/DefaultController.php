<?php

namespace Prodigious\ValidatorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('ProdigiousValidatorBundle:Default:index.html.twig');
    }
}
